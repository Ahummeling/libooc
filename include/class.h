#pragma once
#include "stdlib.h" // for memory allocation
#include "stdio.h"  // for printing errors to the console

typedef struct arg_list arg_list;

typedef struct Class Class;

typedef union class_member {
    char c;
    short s;
    int i;
    long l;
    float f;
    double d;
    void* (*function)(arg_list);
    Class* self;
    void* vp;
} class_member;

typedef struct arg_list {
    int number_of_args;
    class_member* args;
} arg_list;

typedef struct Class {
    void (*__construct)(Class*, arg_list*);
    void (*__destruct)(Class*);
    class_member* members;
    Class* self;
} Class;

extern void __construct(Class*, arg_list*);

extern void __destruct(Class*);

extern Class* new(void (*__construct)(Class*, arg_list*), void (*__destruct)(Class*), arg_list* args);

extern void delete(Class*);

__attribute__((constructor))
static void initialize_class_registry(void);

__attribute__((destructor))
static void free_class_registry(void);

typedef struct class_registry {
    int size;
    Class* classes;
} class_registry;

static class_registry* class_registry_pointer;