# libooc
## WORK IN PROGRESS

Shared library for the implementation of object oriented C.

## Compilation
* Run `make` to compile the object files and link them into the binary executable `client`.
* Run `make clean` to clean the objects folder.
* Run `make <object_name.o>` to compile a specific objct file.

## Structure
* Header files go in the `include` dir.
* C files go in the `src` dir.
* Object files are compiled into the `objects` dir.
* Shared object files are compiled into the `target` dir.
* Tests are placed in the `tests` dir.
* Example projects, using the library, are placed in the `example` dir.

## CI/CD
The CI script runs on the GCC image 10.2.0 and simply calls `make` for the build stage, and `make test` for the test stage.
The build stage lists the `target` dir as artifacts.
<br>
*TODO:* Archive the artifacts into a `.deb` file. And in a later stage, support building compiled `.so` files for multiple distros. (Not for Windows, hyper-v is perfectly capable of running the linux kernel)



### References
* libooc was inspired by this article: https://opensource.com/article/21/2/linux-software-libraries
* idea for usage of gcc attributes was worked out with help from: https://stackoverflow.com/questions/2053029/how-exactly-does-attribute-constructor-work
