#include "class.h"
#include "assert.h"

int main(int argc, char** argv) {
    printf("Starting the libooc testsuite..\n");
    Class* cls = new(__construct, __destruct, NULL);
    assert (cls != NULL);
    delete(cls);
    printf("Tests terminated successfully\n");

    return 0;
}