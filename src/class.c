#include "class.h"

extern Class* new(void (*__construct)(Class*, arg_list*), void (*__destruct)(Class*), arg_list* args) {
    Class* class = malloc(sizeof(Class));
    class->self = class;
    class->__construct = __construct;
    class->__destruct = __destruct;
    __construct(class, args);
    if (NULL != args) {
        free(args);
    }

    return class;
}

extern void delete(Class* class) {
    class->__destruct(class);
    free(class);
}

__attribute__((weak))
extern void __construct(Class* class, arg_list* args) {
    if (NULL != args) {
        fprintf(stderr, "Default constructor cannot be called with arguments\n");
        exit(EXIT_FAILURE);
    }
    class->members = malloc(0*sizeof(class_member));
}

__attribute__((weak))
extern void __destruct(Class* class) {
    if (NULL != class->members) {
        free(class->members);
    }
}


__attribute__((constructor))
static void initialize_class_registry() {
    class_registry_pointer = malloc(sizeof(class_registry));
    if (NULL == class_registry_pointer) {
        fprintf(stderr, "Memory allocation failed before program start\n");
        exit(EXIT_FAILURE);
    }
    class_registry_pointer->size = 0;
    class_registry_pointer->classes = malloc(class_registry_pointer->size*sizeof(Class));
}

__attribute__((destructor))
static void free_class_registry() {
    if (NULL == class_registry_pointer) {
        fprintf(stderr, "Failed to call free on the class registry, memory de-allocation failed.\n");
    }
    free(class_registry_pointer->classes);
    free(class_registry_pointer);
}
