#include "stdio.h"
#include "stdlib.h"
#include "stdarg.h"

#define TYPE_VOID 0
#define TYPE_CHAR 1
#define TYPE_SHORT 2
#define TYPE_INT 3
#define TYPE_LONG 4
#define TYPE_FLOAT 5
#define TYPE_DOUBLE 6

typedef void(*vfni)(int);
typedef void(*vfnc)(char);
typedef void(*vfnd)(double);

typedef struct Argument {
    int type;
    void* value_pointer;
} Argument;

typedef struct Function {
    int return_type;
    Argument* arguments;
    void (*fn)(int*, ...);
} Function;

void test_int(int i) {
    printf("Int is: %i\n", i);
}

void test_char(char c) {
    printf("Char is: %c\n", c);
}

void test_double(double d) {
    printf("Double is: %f\n", d);
}

void call_int(void* value_pointer, va_list v) {
    int* int_value_pointer = (int*) value_pointer;
    int value = *int_value_pointer;
    void (*function_pointer)(int) = va_arg(v, vfni);
    va_end(v);
    (*function_pointer)(value);
}

void call_char(void* value_pointer, va_list v) {
    char* char_value_pointer = (char*) value_pointer;
    char value = *char_value_pointer;
    void (*function_pointer)(char) = va_arg(v, vfnc);
    va_end(v);
    (*function_pointer)(value);
}

void call_double(void* value_pointer, va_list v) {
    double* double_value_pointer = (double*) value_pointer;
    double value = *double_value_pointer;
    void (*function_pointer)(double) = va_arg(v, vfnd);
    va_end(v);
    (*function_pointer)(value);
}

void call(int n, ...) {
    va_list v;
    va_start(v, n);
    Argument argument = va_arg(v, Argument);
    switch(argument.type) {
        case TYPE_INT:
            return call_int(argument.value_pointer, v);
        case TYPE_CHAR:
            return call_char(argument.value_pointer, v);
        case TYPE_DOUBLE:
            return call_double(argument.value_pointer, v);
    }
}

void* va_arg_to_value_pointer(va_list* v, int type) {
    int i;
    char c;
    double d;
    switch (type) {
        case TYPE_INT:
            i = va_arg(*v, int);
            return (void*)(&i);
        case TYPE_CHAR:
            c = va_arg(*v, char);
            return (void*)(&c);
        case TYPE_DOUBLE:
            d = va_arg(*v, double);
            return (void*)(&d);
    }
}

Function create_callback(void (*function_call)(int*, ...), int n, ...) {
    va_list v;
    va_start(v, n);
    Argument* argument_list = malloc((n / 2)*sizeof(Argument));
    int* type_list = malloc((n / 2)*sizeof(int));
    for (int i = 0; i < n / 2; i++) {
        Argument argument;
        argument.type = va_arg(v, int);
        type_list[i] = argument.type;
        argument.value_pointer = va_arg_to_value_pointer(&v, argument.type);
        argument_list[i] = argument;
    }
    Function function;
    function.arguments = argument_list;
    function.return_type = va_arg(v, int);
    function.fn = function_call;
    va_end(v);

    return function;
}

int main(int argc, char** argv) {
    Argument i_arg, c_arg, d_arg;
    int int_value = 5;
    char char_value = 'A';
    double double_value = 3.5;
    i_arg.value_pointer = (void*)(&int_value);
    c_arg.value_pointer = (void*)(&char_value);
    d_arg.value_pointer = (void*)(&double_value);

    i_arg.type = TYPE_INT;
    c_arg.type = TYPE_CHAR;
    d_arg.type = TYPE_DOUBLE;

    call(2, i_arg, test_int);
    call(2, c_arg, test_char);
    call(2, d_arg, test_double);

    Function f = create_callback(test_int, 2, TYPE_INT, 5);
    f.fn(5);


    return 0;
}