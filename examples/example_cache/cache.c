#include "cache.h"

int* cache_get_size(arg_list args) {
    int* r = malloc(sizeof(int));
    r = &args.args[0].self->members[CACHE_MEMBER_SIZE].i;
    return r;
}

void __construct(Class* cache, arg_list* args) {
    cache->members = args->args;
}

Class* cache_class(int size) {
    class_member cache_size;
    cache_size.i = size;
    class_member getter_cache_size;
    getter_cache_size.function = (void*) cache_get_size;
    arg_list* arg_list = malloc(sizeof(arg_list));
    arg_list->number_of_args = CACHE_MEMBERS_TOTAL;
    class_member* members = malloc(CACHE_MEMBERS_TOTAL*sizeof(class_member));
    members[CACHE_MEMBER_SIZE] = cache_size;
    members[CACHE_FUNCTION_GET_SIZE] = getter_cache_size;
    arg_list->args = members;
    
    return new(__construct, __destruct, arg_list);
}