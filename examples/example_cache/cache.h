#pragma once
#include "class.h"

#define CACHE_MEMBERS_TOTAL 2

#define CACHE_MEMBER_SIZE 0

#define CACHE_FUNCTION_GET_SIZE 1

int* cache_get_size(arg_list);

Class* cache_class(int);