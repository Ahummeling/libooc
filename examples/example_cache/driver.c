#include "cache.h"
#include "stdlib.h"

int main(int argc, char** argv) {
    Class* cache = cache_class(atoi(argv[1]));
    int size = cache_get_size(cache);
    printf("Size of the class is %d\n", size);
    delete(cache);

    return 0;
}