#!/bin/bash
LIBDIR="/usr/local/lib/"
INCDIR="/usr/include/"

if [ ! -z $CI ]; then
    SUDO_PREFIX=""
else
    SUDO_PREFIX="sudo "
fi

if [ ! -z $CI_LIB_DIR ]; then
    LIBDIR="$CI_LIB_DIR"
fi

make clean
make
$SUDO_PREFIX mv libooc.so.0.0.0 "$LIBDIR"
$SUDO_PREFIX cp include/class.h $INCDIR
$SUDO_PREFIX ldconfig