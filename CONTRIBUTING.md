# Contributing

Not sure why anyone else would want to spend any time contributing to this, but in general you should probably write your code in such a way that Linus Torvalds would not recommend you to quit programming.

## Code style guidelines
I will try to update this bit as I develop a code style for this library / C.
 - Every `.c` file should only `#include` a header file with the same name.
 - Every function definition should have a matching function declaration in its header file.
 - Parameter names should be omitted in function declarations.
 - Header files should start with `#pragma once`.
 - gcc compiler options should be followed by a newline before the function declaration starts.

### Library specific guidelines
 - Internal functions that are only used by the library should be marked as `static`.
 - External functions that are meant to be used by programs implementing the library should be marked as `extern`.

Demonstrating these rules in a small example:

 ```c
 // example.c
 #include "example.h"
// no more #include preprocessor statements allowed

__attribute__((weak)) // gcc compiler option to allow overriding of this function
extern void say_hello() { // extern void function indicating this function is exposed to the library's api
    library_do_write("Hello, world!");
}

static void library_do_write(char* message) {
    printf("%s\n", message); // copying python3's default newline at the end of a print
}
```

```c
// example.h
#pragma once
#include "stdio.h"

extern void say_hello(void);

static void library_do_write(char*);
```
