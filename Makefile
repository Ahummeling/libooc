IDIR=include
SDIR=src
CC=gcc
CFLAGS=-I $(IDIR) -Wall -Werror -fPIC

ODIR=objects
SODIR=/usr/local/lib

LIBS=

VERSION=0.0.0

.PHONY: move

_DEPS=class.h
DEPS=$(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ=class.o
OBJ=$(patsubst %,$(ODIR)/%,$(_OBJ))

_SO=libooc.so
SO=$(_SO)

$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(SO).$(VERSION): $(OBJ)
	$(CC) -shared -Wl,-soname,$(SO) -o $@ $^ $(CFLAGS) $(LIBS)

move: $(SO).$(VERSION)
	mv $(SO).$(VERSION) /usr/local/lib/

.PHONY: clean
.PHONY: test

clean:
	rm -f $(ODIR)/*.o *~ core $(IDIR)/*~

test:
	cd test && make test